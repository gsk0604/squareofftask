package com.example.homescreen;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnPlayWithFriends;
    private Button btnPlayWithAI;
    private Button btnStreamGames;
    private Toolbar toolbar;

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
        setDynamicButtonSize();

    }

    private void initialize() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        toolbar.setNavigationIcon(R.drawable.menu_black);

        btnPlayWithAI = findViewById(R.id.play_with_ai);
        btnPlayWithFriends = findViewById(R.id.play_with_friends);
        btnStreamGames = findViewById(R.id.stream_games);
    }

    private void setDynamicButtonSize() {
        setButtonSize(btnPlayWithAI);
        setButtonSize(btnPlayWithFriends);
        setButtonSize(btnStreamGames);
        setButtonContent();
    }

    private void setButtonContent() {
        setTextForButton(getResources().getString(R.string.play_with), "AI", btnPlayWithAI);
        setTextForButton(getResources().getString(R.string.play_with), "FRIENDS", btnPlayWithFriends);
        setTextForButton("STREAM", "GAMES", btnStreamGames);
    }


    private void setTextForButton(String first, String second, Button button) {
        Spannable span = new SpannableString(first + "\n" + second);
        span.setSpan(new AbsoluteSizeSpan(14, true), first.length() + 1, first.length() + 1 + second.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        if (button.getId() != btnPlayWithFriends.getId()) {
            span.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.text_highlight_color)), first.length() + 1, first.length() + 1 + second.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        button.setTextSize(8);
        button.setText(span);
        button.setTransformationMethod(null);
    }

    private void setButtonSize(Button button) {
        int size = getScreenWidth() / 2;
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) button.getLayoutParams();
        params.width = size;
        params.height = size;
        params.circleRadius = (int) (size / 1.8);
        button.setLayoutParams(params);
    }
}
